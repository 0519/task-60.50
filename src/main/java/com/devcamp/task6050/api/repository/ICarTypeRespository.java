package com.devcamp.task6050.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task6050.api.model.CCarType;

public interface ICarTypeRespository extends JpaRepository<CCarType,Integer>{
    
}

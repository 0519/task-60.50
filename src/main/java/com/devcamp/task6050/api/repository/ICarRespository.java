package com.devcamp.task6050.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task6050.api.model.CCar;
public interface ICarRespository extends JpaRepository<CCar,Integer> {
    CCar findByCarCode(String car);
    CCar findByCarName(String car);
}

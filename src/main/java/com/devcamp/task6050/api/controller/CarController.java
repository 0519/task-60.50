package com.devcamp.task6050.api.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.task6050.api.model.CCar;
import com.devcamp.task6050.api.model.CCarType;
import com.devcamp.task6050.api.repository.ICarRespository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CarController {
    
    @Autowired
    ICarRespository pCarRespository;

    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<CCar>> getCarList(){

        try {
            List<CCar> pCars = new ArrayList<CCar>();

            pCarRespository.findAll().forEach(pCars::add);
            return new ResponseEntity<>(pCars,HttpStatus.OK);

        } catch (Exception ex ) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp_cartypes")
    public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode")String carCode){
        try {
            CCar vCar = pCarRespository.findByCarCode(carCode);

            if(vCar != null) {
                return new ResponseEntity<>(vCar.getTypes(),HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @GetMapping("/devcamp_cartypes")
    // public ResponseEntity<Set<CCarType>> getCarTypeByCarName(@RequestParam(value = "carName")String carName){
    //     try {
    //         CCar vCar = pCarRespository.findByCarName(carName);

    //         if(vCar != null) {
    //             return new ResponseEntity<>(vCar.getTypes(),HttpStatus.OK);
    //         } else {
    //             return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
    //         }
    //     } 
    //     catch (Exception ex) {
    //         return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
    //     }
    // }
}

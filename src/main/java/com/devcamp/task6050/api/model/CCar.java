package com.devcamp.task6050.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.GenerationType;

import java.util.Set;

import javax.persistence.CascadeType;
@Entity
@Table(name = "car")
public class CCar {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int carId;

    @Column(name = "car_code")
    private String carCode;

    @Column(name = "car_name")
    private String carName;

    @OneToMany(mappedBy = "car" , cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CCarType> types;

    public CCar(){

    }

    public CCar(int carId, String carCode, String carName, Set<CCarType> types) {
        this.carId = carId;
        this.carCode = carCode;
        this.carName = carName;
        this.types = types;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Set<CCarType> getTypes() {
        return types;
    }

    public void setTypes(Set<CCarType> types) {
        this.types = types;
    }

}

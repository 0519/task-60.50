package com.devcamp.task6050.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "car_type")
public class CCarType {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int typeId;

    @Column(name = "type_code")
    private String typeCode;

    @Column(name = "type_name")
    private String typeName;

    @ManyToOne
    @JoinColumn(name = "car_id")
    @JsonBackReference
    private CCar car;

    public CCarType(){

    }

    public CCarType(int typeId, String typeCode, String typeName, CCar car) {
        this.typeId = typeId;
        this.typeCode = typeCode;
        this.typeName = typeName;
        this.car = car;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public CCar getCar() {
        return car;
    }

    public void setCar(CCar car) {
        this.car = car;
    }

    
}
